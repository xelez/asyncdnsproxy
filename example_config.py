BLACKLIST = [
    'aa.cc', # exact match (without subdomains)
    '*.perm.ru',  # all subdomains (but not perm.ru!)
    'ad.some*.*', # custom glob
]

UPSTREAM = ('8.8.8.8', 53)

UPSTREAM_TIMEOUT = 5.0 # in seconds

# returns NXDOMAIN for blocked domains
# BLACKLIST_ANSWER = 'NOT_FOUND'

# returns static ip for blocked domains
# BLACKLIST_ANSWER = {
#     'ipv4' : '1.1.1.1',
#     'ipv6' : '::2',
#     'ttl' : 60
# }
# some or all values may be omitted to answer for only A or AAAA records

# for example
BLACKLIST_ANSWER = {'ipv4' : '2.2.2.2'}