from dnslib import DNSRecord, RR, RCODE

from asyncdnsproxy.responders import NotFoundResponder, StaticIPResponder


def test_not_found_responder():
    sut = NotFoundResponder()
    r = sut.respond(DNSRecord.question('example.com'))
    assert r.header.rcode == RCODE.NXDOMAIN


def test_static_ip_responds_with_A():
    sut = StaticIPResponder('127.0.0.1', ttl=60)
    r = sut.respond(DNSRecord.question('aa.example.com'))
    assert r.rr == RR.fromZone('aa.example.com. 60 IN A 127.0.0.1')


def test_static_ip_responds_with_AAAA():
    sut = StaticIPResponder(ipv6='::2', ttl=70)
    r = sut.respond(DNSRecord.question('bb.example.com', qtype="AAAA"))
    assert r.rr == RR.fromZone('bb.example.com. 70 IN AAAA ::2')


def test_static_ip_responds_with_A_and_AAAA():
    sut = StaticIPResponder(ipv4='1.1.1.1', ipv6='::2', ttl=10)
    r = sut.respond(DNSRecord.question('a', qtype="ANY"))
    zone = 'a. 10 IN A 1.1.1.1\na. 10 IN AAAA ::2'
    assert r.rr == RR.fromZone(zone)


def test_dont_respond_to_other_zones():
    sut = StaticIPResponder('127.0.0.1', ttl=60)
    r = sut.respond(DNSRecord.question('bind.version', qclass='CS'))
    assert r.header.rcode == RCODE.NOERROR
    assert r.rr == []
