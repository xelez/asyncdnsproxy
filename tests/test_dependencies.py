import pytest
import binascii

from dnslib import DNSRecord, DNSQuestion, DNSLabel


def test_parse_packet():
    packet_bytes = binascii.unhexlify(b'f163010000010000000000000279610272750000010001')
    packet = DNSRecord.parse(packet_bytes)
    assert packet.header.id == 0xf163  # transaction id
    assert packet.header.qr == 0  # is query (0) or response (1)
    assert packet.header.opcode == 0  # standard query
    assert len(packet.questions) == 1
    assert len(packet.rr) == 0
    assert len(packet.auth) == 0
    assert len(packet.ar) == 0


def test_dns_one_question():
    q = DNSRecord.question('ya.ru')
    r = q.send('8.8.8.8')
    p = DNSRecord.parse(r)
    assert len(p.questions) == 1
    assert len(p.rr) > 0


@pytest.mark.xfail(reason='Not supported by DNS servers')
def test_dns_multiple_questions():
    q = DNSRecord.question('yandex.ru')
    q.add_question(DNSQuestion('google.ru'))
    r = q.send('8.8.8.8', timeout=2)
    p = DNSRecord.parse(r)
    assert len(p.questions) == 2


def test_utf_domain():
    l1 = DNSLabel('тест.рф')
    l2 = DNSLabel('Тест.рф')
    assert str(l1) == 'xn--e1aybc.xn--p1ai.'
    assert str(l2) == 'xn--e1aybc.xn--p1ai.'


def test_decode_uppercase_domain():
    r = DNSRecord.question('ExampLe.Com')
    assert str(r.q.qname) == 'ExampLe.Com.'
    p = DNSRecord.parse(r.pack())
    assert str(p.q.qname) == 'ExampLe.Com.'
