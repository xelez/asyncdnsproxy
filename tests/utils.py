import asyncio

class UdpEchoProtocol(asyncio.DatagramProtocol):
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        self.transport.sendto(data, addr)

class TcpEchoProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        self.transport.write(data)
        self.transport.close()

class BadTcpEchoProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        self.transport.write(b'ko')


def start_udp_server(loop, local_addr):
    return loop.create_datagram_endpoint(UdpEchoProtocol, local_addr=local_addr)

def start_tcp_server(loop, server_addr):
    return loop.create_server(TcpEchoProtocol, *server_addr)

def start_bad_tcp_server(loop, server_addr):
    return loop.create_server(BadTcpEchoProtocol, *server_addr)