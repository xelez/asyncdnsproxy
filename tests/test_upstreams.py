import asyncio

import pytest

from asyncdnsproxy.upstreams import send_udp, send_tcp, UpstreamError, Upstream
from tests.utils import start_udp_server, start_tcp_server, start_bad_tcp_server


@pytest.fixture()
def server_addr():
    return '127.0.0.1', 9999


@pytest.mark.asyncio
async def test_send_udp(event_loop, server_addr):
    t, p = await start_udp_server(event_loop, server_addr)
    msg = await asyncio.wait_for(send_udp(b'abacaba', *server_addr), timeout=2)
    assert msg == b'abacaba'
    t.close()


@pytest.mark.asyncio
async def test_send_tcp(event_loop, server_addr):
    server = await start_tcp_server(event_loop, server_addr)
    msg = await asyncio.wait_for(send_tcp(b'testmsg', *server_addr), timeout=5.0)
    assert msg == b'testmsg'
    server.close()


@pytest.mark.asyncio
async def test_send_tcp_with_bad_server(event_loop, server_addr):
    server = await start_bad_tcp_server(event_loop, server_addr)
    with pytest.raises(asyncio.TimeoutError):
        _ = await asyncio.wait_for(send_tcp(b'testmsg', *server_addr), timeout=1.0)
    server.close()


@pytest.mark.asyncio
async def test_upstream_timeouts(event_loop, server_addr):
    server = await start_bad_tcp_server(event_loop, server_addr)
    upstream = Upstream(*server_addr, timeout=1.0)
    with pytest.raises(UpstreamError):
        _ = await asyncio.wait_for(upstream.pass_packet(b'data'), timeout=3.0)
    server.close()
