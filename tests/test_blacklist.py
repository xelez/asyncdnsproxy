import pytest

from asyncdnsproxy.blacklist import Blacklist


@pytest.fixture()
def blacklist():
    return Blacklist()


def test_good_domain(blacklist):
    blacklist.add('example.com')
    assert 'example2.com' not in blacklist


def test_bad_domain(blacklist):
    blacklist.add('example.com')
    assert 'example.com' in blacklist


def test_recognizes_different_spelling(blacklist):
    blacklist.add('aa.bb')
    assert 'aa.bb' in blacklist
    assert 'aA.bb' in blacklist
    assert 'aa.bb.' in blacklist


@pytest.mark.xfail(reason="bug in dnslib, but we don't really care for this case")
def test_point_before_domain(blacklist):
    assert '.aa.bb' in blacklist


def test_recognizes_russian_domains(blacklist):
    blacklist.add('тест.рф')
    assert 'xn--e1aybc.xn--p1ai' in blacklist
    assert 'тест.рф' in blacklist
    assert 'тЕст.РФ' in blacklist


def test_blocks_subtree(blacklist):
    blacklist.add('*.example.com')
    assert 'aa.example.com' in blacklist
    assert 'bb.cc.example.com' in blacklist


def test_doesnt_block_similar_names(blacklist):
    blacklist.add('*.aa.bb')
    assert 'aaa.bb' not in blacklist


def test_blocks_only_subtree(blacklist):
    blacklist.add('bb.cc')
    blacklist.add('*.aa.bb.cc')
    assert 'aa.bb.cc' not in blacklist


def test_glob_pattern(blacklist):
    blacklist.add('a*.bb')
    assert 'aa.bb' in blacklist