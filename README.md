# Async DNS Proxy Server with blacklist support

Written using dnslib and asyncio.


## Dependencies

You can install all dependencies using:

    pip install -r requirements.txt


## Running

```
$ python run.py -h
usage: run.py [-h] [-c CONFIG] [-b BIND] [-p PORT]

Proxing DNS server with blacklist.

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        path to config file
  -b BIND, --bind BIND  the address for the server to bind
  -p PORT, --port PORT  change port to bind
```

Remember that you need privileges to bind on port 53 (for example, run as root or use sudo):

    # python run.py -c example_config.py


Dropping privileges is not implemented at the moment.

## Configuration

See `example_config.py` for avaliable options

## Test suite

You can run tests. Beware, some of them use ports 5353 and 9999 on 127.0.0.1 (they should be unused on the system for tests to succeed).
And some tests may require access to DNS on 8.8.8.8.

You can run tests with pytest:

```
$ pytest 
========================== test session starts ==========================
platform linux -- Python 3.6.1, pytest-3.2.3, py-1.4.34, pluggy-0.4.0
rootdir: /home/xelez/projects/find_work/filtering_dns/project, inifile:
plugins: asyncio-0.8.0
collected 23 items                                                       

tests/test_blacklist.py ...x.....
tests/test_dependencies.py ..x..
tests/test_responders.py .....
tests/test_upstreams.py ....

================= 21 passed, 2 xfailed in 2.29 seconds ==================
```
