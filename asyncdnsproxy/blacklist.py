import fnmatch
from dnslib import DNSLabel


def _format_domain(name):
    return str(DNSLabel(name)).lower()


def _is_pattern(name):
    return any(c in {'*', '?', '[', ']'} for c in name)


class Blacklist:
    def __init__(self, domains=None):
        self._exact_blacklist = set()
        self._suffix_blacklist = set()
        self._patterns = []

        if domains:
            self.extend(domains)

    def add(self, domain):
        name = _format_domain(domain)

        if name.startswith('*.') and not _is_pattern(name[2:]):
            self._suffix_blacklist.add(name[2:])
        elif _is_pattern(name):
            self._patterns.append(name)
        else:
            self._exact_blacklist.add(name)

    def extend(self, domains):
        for domain in domains:
            self.add(domain)

    def __contains__(self, domain):
        name = _format_domain(domain)
        if name in self._exact_blacklist:
            return True

        for i in range(-len(name), -1):
            if (name[i] == '.') and name[i+1:] in self._suffix_blacklist:
                return True

        for pattern in self._patterns:
            if fnmatch.fnmatch(name, pattern):
                return True

        return False
