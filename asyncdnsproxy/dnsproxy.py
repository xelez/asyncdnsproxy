import logging
from dnslib import DNSRecord

from .responders import Responder
from .upstreams import Upstream
from .blacklist import Blacklist


class DNSProxy:

    def __init__(self, upstream: Upstream, blacklist: Blacklist, blacklist_responder: Responder):
        self.upstream = upstream
        self.blacklist = blacklist
        self.blacklist_responder = blacklist_responder

    async def try_handle_request(self, data: bytes, max_response_len=None):
        query = 'Not parsed'
        try:
            query = DNSRecord.parse(data)
            response = await self.handle_request(query, data)

            if max_response_len and len(response) > max_response_len:
                response = query.reply().truncate().pack()

            return response
        except Exception as e:
            logging.exception('Error handling request:\n'+str(query))
            return None

    def log_strange_queries(self, query: DNSRecord):
        if query.header.qr != 0:
            logging.warning('Got response instead of request:\n' + str(query))
        if len(query.questions) != 1:
            logging.warning('Number of questions != 1:\n' + str(query))
        if query.header.opcode != 0:
            logging.warning('Got not standard query:\n' + str(query))

    async def handle_request(self, query: DNSRecord, data: bytes) -> bytes:
        self.log_strange_queries(query)

        if len(query.questions) > 0 and query.q.qname in self.blacklist:
            response = self.blacklist_responder.respond(query)
            return response.pack()
        else:
            return await self.upstream.pass_packet(data)
