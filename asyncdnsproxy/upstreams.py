import asyncio
import socket
import struct

from .utils import sock_recvfrom


class UpstreamError(Exception):
    pass


async def send_udp(data, host, port):
    """
        Helper function to send/receive DNS UDP request
    """
    loop = asyncio.get_event_loop()
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.setblocking(False)
        sock.sendto(data, (host, port))
        response, server = await sock_recvfrom(loop, sock, 8192)
    return response


async def send_tcp(data, host, port):
    """
        Helper function to send/receive DNS TCP request
        (in/out packets will have prepended TCP length header)
    """
    loop = asyncio.get_event_loop()
    data = struct.pack("!H", len(data)) + data

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setblocking(False)
        await loop.sock_connect(sock, (host, port))
        await loop.sock_sendall(sock, data)
        response = await loop.sock_recv(sock, 8192)
        if len(response) < 2:
            raise UpstreamError('response is too short')
        length = struct.unpack("!H", bytes(response[:2]))[0]
        while len(response) - 2 < length:
            part = await loop.sock_recv(sock, 8192)
            if not len(part):
                raise UpstreamError('response is too short')
            response += part

    return response[2:]


class Upstream:
    def __init__(self, host, port=53, timeout=4.0, send_func=send_udp):
        self.server_addr = (host, port)
        self.timeout = timeout
        self.send_func = send_func

    async def pass_packet(self, data):
        try:
            return await asyncio.wait_for(self.send_func(data, *self.server_addr), timeout=self.timeout)
        except UpstreamError as e:
            raise e
        except Exception as e:
            raise UpstreamError("Upstream error") from e
