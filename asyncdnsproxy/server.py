import asyncio
import struct

from .dnsproxy import DNSProxy


class DNSDatagramProtocol(asyncio.DatagramProtocol):

    def __init__(self, proxy: DNSProxy):
        self.proxy = proxy
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        asyncio.ensure_future(self.handle_request(data, addr))

    async def handle_request(self, data, addr):
        response = await self.proxy.try_handle_request(data, max_response_len=512)
        if response:
            self.transport.sendto(response, addr)


class DNSProtocol(asyncio.Protocol):

    def __init__(self, proxy: DNSProxy):
        self.proxy = proxy
        self.transport = None
        self.length = None
        self.data = b''

    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        self.data += data

        while True:
            if len(self.data) < 2:
                return

            length = struct.unpack("!H", self.data[:2])[0]

            if len(self.data) < length + 2:
                return

            request, self.data = self.data[2:length + 2], self.data[length + 2:]
            asyncio.ensure_future(self.handle_request(request))

    async def handle_request(self, data):
        response = await self.proxy.try_handle_request(data, max_response_len=512)
        if response:
            response = struct.pack("!H", len(response)) + response
            self.transport.write(response)


async def start_udp_server(proxy, host='0.0.0.0', port=53):
    loop = asyncio.get_event_loop()
    return await loop.create_datagram_endpoint(lambda: DNSDatagramProtocol(proxy), local_addr=(host, port))


async def start_tcp_server(proxy, host='0.0.0.0', port=53):
    loop = asyncio.get_event_loop()
    return await loop.create_server(lambda: DNSProtocol(proxy), host, port)
