from dnslib import DNSRecord, RR
from dnslib import RCODE, QTYPE, CLASS, A, AAAA


class Responder:
    def respond(self, query: DNSRecord) -> DNSRecord:
        raise NotImplementedError()


class NotFoundResponder(Responder):
    def respond(self, query: DNSRecord) -> DNSRecord:
        r = query.reply()
        r.header.rcode = RCODE.NXDOMAIN
        return r


class StaticIPResponder(Responder):
    def __init__(self, ipv4=None, ipv6=None, ttl=60):
        self.ipv4 = ipv4
        self.ipv6 = ipv6
        self.ttl = ttl

    def respond(self, query: DNSRecord) -> DNSRecord:
        r = query.reply()
        q = query.q

        if q.qclass == CLASS.IN:
            if self.ipv4 and (q.qtype in (QTYPE.A, QTYPE.ANY)):
                rr = RR(q.qname, QTYPE.A, CLASS.IN, ttl=self.ttl, rdata=A(self.ipv4))
                r.add_answer(rr)

            if self.ipv6 and (q.qtype in (QTYPE.AAAA, QTYPE.ANY)):
                rr = RR(q.qname, QTYPE.AAAA, CLASS.IN, ttl=self.ttl, rdata=AAAA(self.ipv6))
                r.add_answer(rr)

        return r
