import argparse
import asyncio
import logging

from asyncdnsproxy.config import Config
from asyncdnsproxy.dnsproxy import DNSProxy
from asyncdnsproxy.responders import NotFoundResponder, StaticIPResponder
from asyncdnsproxy.server import start_udp_server, start_tcp_server
from asyncdnsproxy.upstreams import Upstream, send_tcp

from asyncdnsproxy.blacklist import Blacklist

default_config = {
    'BLACKLIST': [],
    'UPSTREAM': ('8.8.8.8', 53),
    'UPSTREAM_TIMEOUT': 5,
    'BLACKLIST_ANSWER': 'NOT_FOUND',
    'BIND_TO': ('127.0.1.1', 53)
}


def setup_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)


def parse_cmdline():
    parser = argparse.ArgumentParser(description='Proxing DNS server with blacklist.')
    parser.add_argument('-c', '--config', default=None,
                        help='path to config file')
    parser.add_argument('-b', '--bind', default='0.0.0.0',
                        help='the address for the server to bind')
    parser.add_argument('-p', '--port', type=int, default=53,
                        help='change port to bind')

    return parser.parse_args()


async def start(config):
    blacklist = Blacklist(config['BLACKLIST'])
    if config['BLACKLIST_ANSWER'] == 'NOT_FOUND':
        blacklist_responder = NotFoundResponder()
    else:
        blacklist_responder = StaticIPResponder(**config['BLACKLIST_ANSWER'])

    udp_upstream = Upstream(*config['UPSTREAM'], float(config['UPSTREAM_TIMEOUT']))
    tcp_upstream = Upstream(*config['UPSTREAM'], float(config['UPSTREAM_TIMEOUT']), send_func=send_tcp)

    udp_proxy = DNSProxy(udp_upstream, blacklist, blacklist_responder)
    tcp_proxy = DNSProxy(tcp_upstream, blacklist, blacklist_responder)

    udp_transport, _ = await start_udp_server(udp_proxy, *config['BIND_TO'])
    tcp_server = await start_tcp_server(tcp_proxy, *config['BIND_TO'])

    return udp_transport, tcp_server


def main():
    setup_logging()
    config = Config(default_config)
    args = parse_cmdline()

    config['BIND_TO'] = (args.bind, args.port)
    if args.config:
        config.from_pyfile(args.config)

    loop = asyncio.get_event_loop()
    udp_transport, tcp_server = loop.run_until_complete(start(config))

    logging.info('Listing on {}:{} (udp)'.format(*(udp_transport.get_extra_info('sockname')[:2])))
    for sock in tcp_server.sockets:
        logging.info('Listing on {}:{} (tcp)'.format(*(sock.getsockname()[:2])))

    try:
        loop.run_forever()
    except Exception:
        pass

    tcp_server.close()
    udp_transport.close()
    loop.run_until_complete(tcp_server.wait_closed())
    loop.close()


if __name__ == '__main__':
    main()
